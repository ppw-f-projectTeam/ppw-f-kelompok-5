from django import forms

class Testimony_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input yang kosong',
        'invalid': 'Isi input dengan benar',
    }

    testimony = forms.CharField(label='Testimoni', required=True,
                           widget=forms.Textarea(attrs={'class': 'form-control'}))
