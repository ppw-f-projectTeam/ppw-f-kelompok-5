from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
import unittest


# Create your tests here.

class AboutUnitTest(TestCase):
    def about_page_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_using_register_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_using_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about/about.html')

    def test_models_work(self):
        Testimony.objects.create(testimony="I BID YOU GOOD DAY SIR", username="Axe")
        amount = Testimony.objects.all().count()
        self.assertEqual(amount, 1)

    def test_can_save_POST_request(self):
        response = self.client.post('/about/',
                                    data={'testimony': "OMAE WA MOU SHINDEIRU"})
        insert = Testimony.objects.all().count()
        self.assertEqual(insert, 1)
        self.assertEqual(response.status_code, 302)
