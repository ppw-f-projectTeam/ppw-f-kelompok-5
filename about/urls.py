from django.urls import path
from . import views

urlpatterns = [
    path('', views.about, name='about'),
    path('list/', views.about_list, name="about_list")
]
