from django.shortcuts import render, redirect
from django.http import  JsonResponse
from .models import *
from .forms import Testimony_Form

# Create your views here.
def about(request):
    user = request.user
    if request.method == "POST":
        form = Testimony_Form(request.POST)
        if form.is_valid():
            testimony = request.POST['testimony']
            username = user.username
            testim = Testimony(testimony=testimony, username=username)
            testim.save()
            return redirect('about')
    else:
        form = Testimony_Form()
    return render(request, "about/about.html",{'form': form} )

def about_list(request):
    raw = Testimony.objects.all()
    jlist = [{'testimony': obj.testimony, 'username': obj.username} for obj in raw]
    return JsonResponse(jlist, safe=False)

