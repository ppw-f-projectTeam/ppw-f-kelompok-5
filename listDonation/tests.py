from django.test import TestCase, Client
from django.urls import resolve
from .views import myDonation
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib import auth

# Create your tests here.
class DonationTest(TestCase):
    def test_website_donasi_saya_url_is_redirect(self):
        response = Client().get('/donasi-saya/')
        self.assertEqual(response.status_code,302)
    
    def test_website_donasi_saya_url_is_exist(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        client=Client()
        client.login(username='testuser', password='12345')
        response = client.get('/donasi-saya/')
        self.assertEqual(response.status_code,200)

    def test_website_data_donasi_saya_url_is_redirect(self):
        response = Client().get('/donasi-saya/data/')
        self.assertEqual(response.status_code,302)

    def test_website_data_donasi_saya_url_is_exist(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        client=Client()
        client.login(username='testuser', password='12345')
        response = client.get('/donasi-saya/data/')
        self.assertEqual(response.status_code,200)