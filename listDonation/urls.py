from django.urls import path
from . import views

urlpatterns = [
    path('', views.myDonation, name="myDonation"),
    path('data/', views.dataDonation, name="dataDonation")
]