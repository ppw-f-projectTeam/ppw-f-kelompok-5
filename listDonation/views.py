from django.shortcuts import render, redirect
from submitForm.models import Donasi
from django.http import JsonResponse
# Create your views here.
def myDonation(request):
    if (request.user.is_authenticated):
        return render(request, "donationSaya.html", {})
    else:
        return redirect("/registerForm/")


def dataDonation(request):
    if (request.user.is_authenticated):
        email = request.user.email
        allDonasi = Donasi.objects.all().filter(email=email)
        response = {"daftar_donasi" : list()}
        total_donasi = 0

        dict_donasi = dict()
        for donasi in allDonasi:
            if(donasi.nama_program in dict_donasi):
                dict_donasi[donasi.nama_program] += donasi.uang_donasi
            else:
                dict_donasi[donasi.nama_program] = donasi.uang_donasi

        for donasi in dict_donasi:
            response["daftar_donasi"].append({"nama_program" : donasi, "uang_donasi" : dict_donasi[donasi]})
            total_donasi += dict_donasi[donasi]
        
        response["total_donasi"] = total_donasi
        return JsonResponse(response)
    else:
        return redirect("/registerForm/")