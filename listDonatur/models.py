from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save
from django.utils.text import slugify

# Create your models here.
class News(models.Model):
    author = models.CharField(max_length=50)
    title = models.CharField(max_length=150)
    picture = models.CharField(max_length=150)
    tipe = models.CharField(max_length=150, default='news')
    category = models.CharField(max_length=150, default='none')
    smallDescription = models.CharField(max_length=100, default='none')
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    slug = models.SlugField(unique=True)
        
    def __str__(self):
        return self.title
