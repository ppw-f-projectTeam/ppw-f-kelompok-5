from django.test import TestCase, Client
from django.urls import resolve
from .views import newsDetail, newsDisplay
from .models import News
from django.http import HttpRequest

class WebsiteTest(TestCase):
    def test_website_news_url_is_exist(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code,200)

    def test_website_news_using_to_do_list_template(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'listDonatur/newsDisplay.html')

    def test_website_news_detail(self):
        berita = News.objects.create(
            slug = 'hello-world', 
            category = 'test',
            title="test",
            author='Wildan',
            text='This is test',
            picture='picture.jpg')
        response = Client().get('/berita/news/test/hello-world/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code,200)
        counting_all_available_todo = News.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
        self.assertTemplateUsed(response, 'listDonatur/newsClick.html')
        self.assertEqual(str(berita),'test')