from django.urls import path
from . import views

urlpatterns = [
    path('', views.newsDisplay, name='newsDisplay'),
    path('news/<category>/<slug:slug_value>/', views.newsDetail, name="newsDetail")
]
