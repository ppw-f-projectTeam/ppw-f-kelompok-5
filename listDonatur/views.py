from django.shortcuts import render
from .models import News
from programList.models import programData

# Create your views here.

def newsDisplay(request):
    allNews = News.objects.all().order_by('created_date')
    headlineNews = News.objects.filter(tipe='headline')
    return render(request, "listDonatur/newsDisplay.html", {'allNews': allNews, 'headlineNews': headlineNews})

def newsDetail(request, category, slug_value):
    selectedNews = News.objects.get(slug = slug_value)
    selectedProgram = programData.objects.filter(namaProgram__contains=category)
    return render(request, "listDonatur/newsClick.html", {'selectedNews': selectedNews, 'selectedProgram' : selectedProgram})