
$(function(){
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $('#form').on('submit', function(event){
        event.preventDefault();
        create_testimony();
    });

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function create_testimony() {
        console.log("its working!") // sanity check
        var testimony = $("#id_testimony").val();

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $.ajax({
            url : about, // the endpoint
            type : "POST", // http method
            data : { 'testimony' : testimony, 'username' : username}, // data sent with the post request
            // handle a successful response
            success : function(data) {
                alert("You are successfully entered your testimony!, " + username);
                window.location.reload();
            },
            error : function(data) {
                alert("There's an error going on!");
            }
        });

        $('.listed').empty()
        $.ajax({
            url : list,
            dataType : "json",
            success : function (datajson) {
                var testimonies = datajson;
                console.log(datajson);
                for (var i = 0; i < testimonies.length; i++) {
                    var table_content = "<tr>" +
                        "<td>" + testimonies[i].testimony + "</td>" +
                        "<td>" + testimonies[i].username + "</td>" +
                    "</tr>";
                    $('.listed').append(table_content);
                };
            },

            error: function (error) {
                var table_content = "<tr><td colspan=\"5\" class = \"text-center\">JSON failed to loaded :(</td></tr>";
                $('.listed').append(table_content);
            },
        });
    };


});