from django.apps import AppConfig


class ProgramlistConfig(AppConfig):
    name = 'programList'
