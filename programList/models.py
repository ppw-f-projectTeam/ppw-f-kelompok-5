from django.db import models

#models.py

Kategori_Opsi = (
   ('Bencana Alam', 'Bencana Alam'),
   ('Kegiatan Sosial', 'Kegiatan Sosial'),
   ('Bantuan Medis', 'Bantuan Medis'))


# Create your models here.
class programData(models.Model) :
    namaProgram = models.CharField(max_length=120)
    linkGambar = models.CharField(max_length=120)
    kategori = models.CharField( max_length=128, choices=Kategori_Opsi, default='Bencana Alam')
    deskripsiBencana = models.CharField(max_length=1200)
