from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import programData
from django.http import HttpRequest


class testProgramList(TestCase):

    def setUp(self):
        programData.objects.create(namaProgram= "Bantuan Pemerintah",
                                   linkGambar= "https://i.postimg.cc/J05tjfkn/Gempa-Palu.jpg",
                                   deskripsiBencana= "Bencana X terjadi di Y pada tanggal Z kemarin"
                                   )
# Create your tests here.
    def test_url_base_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_data_saved_in_database(self):
        jumlah = programData.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_page_render_templates_expected(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "programList/program.html")

    def test_if_html_contain_data_in_models(self):
        judul = 'judul berita'
        programData.objects.create(namaProgram=judul,
                                   linkGambar="https://i.postimg.cc/J05tjfkn/Gempa-Palu.jpg",
                                   deskripsiBencana="Bencana X terjadi di Y pada tanggal Z kemarin"
                                   )
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn(judul, html_response)
