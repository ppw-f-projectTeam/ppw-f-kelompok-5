from django.shortcuts import render
from .models import programData

# Create your views here.

def index(request):
    datas = programData.objects.all()
    response = {'datas': datas}
    return render(request, "programList/program.html", response)
