# Generated by Django 2.1.1 on 2018-12-06 04:33

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('registerForm', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='register',
            name='birthDate',
        ),
        migrations.RemoveField(
            model_name='register',
            name='email',
        ),
        migrations.RemoveField(
            model_name='register',
            name='name',
        ),
        migrations.RemoveField(
            model_name='register',
            name='password',
        ),
        migrations.AddField(
            model_name='register',
            name='bio',
            field=models.TextField(blank=True, max_length=500),
        ),
        migrations.AddField(
            model_name='register',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='register',
            name='fullname',
            field=models.CharField(blank=True, max_length=30),
        ),
        migrations.AddField(
            model_name='register',
            name='username',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
