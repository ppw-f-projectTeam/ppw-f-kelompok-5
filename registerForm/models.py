from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

class Register(models.Model):
    user = models.OneToOneField(User, unique=True, null=False, db_index=True, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    fullname = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
#
# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Register.objects.create(user=instance)
#     #
#     # @receiver(post_save, sender=User)
#     # def save_user_profile(sender, instance, **kwargs):
#     #     instance.register.save()
#
#

# TEPE SATU
# class Register(models.Model):
#     name = models.CharField(max_length=50)
#     birthDate = models.DateField(auto_now=True)
#     email = models.EmailField(unique=True)
#     password = models.CharField(max_length=30)
