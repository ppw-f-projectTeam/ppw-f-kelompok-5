from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from django.urls import resolve
from .views import *
from .models import Register
from django.http import HttpRequest
from datetime import datetime
import unittest


# Create your tests here.
class RegisterFormUnitTest(TestCase):
    def test_register_page_exist(self):
        response = Client().get('/account/login/')
        self.assertEqual(response.status_code, 200)


