from django.urls import path
from . import views
from django.conf import settings

urlpatterns = [
    path('', views.register, name='register'),
    path('account/logout/', views.Logout, {'next_page': settings.LOGOUT_REDIRECT_URL}),
]
