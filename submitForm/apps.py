from django.apps import AppConfig


class SubmitformConfig(AppConfig):
    name = 'submitForm'
