from django import forms
from .models import *

class FormDonasi(forms.ModelForm):
    # nama = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    # email = forms.EmailField(max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    uang_donasi = forms.IntegerField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    sebagai_anonim = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class' : 'form-group form-check'}))
    
    
    class Meta:
        model = Donasi
        fields = ('uang_donasi','sebagai_anonim')
        # fields = ('nama','email','uang_donasi','sebagai_anonim')
