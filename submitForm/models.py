from django.db import models
from django.utils.timezone import now

# Create your models here.

class Donasi(models.Model):

    datetime = models.DateTimeField(default=now, blank=True)
    nama = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    uang_donasi = models.IntegerField()
    nama_program = models.CharField(max_length=200)
