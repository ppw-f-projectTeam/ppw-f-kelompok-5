from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .views import *
from .models import *

from programList.models import programData
from registerForm.models import Register
from django.contrib.auth.models import User


class TestSubmitForm(TestCase):

    def test_donate_url_is_exist(self):
        programDonasi = programData.objects.create(namaProgram='gempalombok', linkGambar='x', kategori='Bencana Alam', deskripsiBencana='x')
        response = Client().get('/submitForm/donate/gempalombok')
        self.assertEqual(response.status_code, 200)

    # def test_if_email_registered_and_donate_succes(self):
    #     programDonasi = programData.objects.create(namaProgram='gempalombok', linkGambar='x', kategori='Bencana Alam', deskripsiBencana='x')
        
    #     user = User.objects.create(username='testuser', email="user@gmail.com")
    #     user.set_password('12345')
    #     user.save()
    #     client=Client()
    #     client.login(username='testuser', password='12345')

    #     response = self.client.post('/submitForm/donate/gempalombok',
    #                                 data={
    #                                       'uang_donasi': 123
    #                                       })
    #     self.assertEqual(response.status_code, 302)

    # def test_if_email_not_registered_and_donate_failed(self):
    #     programDonasi = programData.objects.create(namaProgram='gempalombok', linkGambar='x', kategori='Bencana Alam', deskripsiBencana='x')

    #     response = self.client.post('/submitForm/donate/gempalombok',
    #                                 data={
    #                                       'nama': "budi",
    #                                       'email': "budi@gmail.com",
    #                                       'uang_donasi': 123
    #                                       })
    #     self.assertEqual(response.status_code, 302)
    #     self.assertRedirects(response, '/registerForm/')
        

    def test_listdonatur_url_is_exist(self):
        programDonasi = programData.objects.create(namaProgram='gempalombok', linkGambar='x', kategori='Bencana Alam', deskripsiBencana='x')
        
        response = Client().get('/submitForm/donate/listdonatur/gempalombok')
        self.assertEqual(response.status_code, 200)
