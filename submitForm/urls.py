from django.urls import path
from .views import *
from . import views

urlpatterns = [
    path('donate/<program>', donate, name='donate'),
    path('donate/listdonatur/<program>', listdonatur_func, name='listdonatur_page'),
    
]

