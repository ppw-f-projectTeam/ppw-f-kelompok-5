from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from .models import *
from . import forms
from programList.models import programData
from registerForm.models import Register

# Create your views here.

def donate(request, program=None):

    details = programData.objects.all().filter(namaProgram=program)

    if request.method == "POST":
        form = forms.FormDonasi(request.POST)
        if form.is_valid():

            nama = "**Orang Dermawan**"
            email = request.user.email
            uang_donasi = request.POST['uang_donasi']

            try:
                sebagai_anonim = request.POST['sebagai_anonim']
            except:
                nama = request.user.first_name + " " + request.user.last_name

            # Donasi
            donasi = Donasi(nama=nama, email=email, uang_donasi=uang_donasi, nama_program=program)
            donasi.save()

            return redirect('donate', program)
            
    else:
        form = forms.FormDonasi()

    response = {'program' : details[0],'form' : form}
    return render(request, 'submitForm/submitform.html', response)

    # details = programData.objects.all().filter(namaProgram = program)

    # if request.method == "POST":
    #     form = forms.FormDonasi(request.POST)
    #     if form.is_valid():

    #         email = request.POST['email']
    #         listEmail = Register.objects.all().filter(email = email)

    #         if (len(listEmail) == 0):
    #             messages.error(request,"Email belum terdaftar")
    #             return redirect('register')

    #         else:
    #             nama = '**Orang Dermawan**'

    #             try:
    #                 sebagai_anonim = request.POST['sebagai_anonim']
    #             except:
    #                 nama = request.POST['nama']

    #             uang_donasi = request.POST['uang_donasi']

    #             # Donasi
    #             donasi = Donasi(nama=nama, email=email, uang_donasi=uang_donasi, nama_program=program)
    #             donasi.save()
    #             messages.success(request,"Anda berhasil melakukan donasi.")

    #             return redirect('donate', program)
            
    # else:
    #     form = forms.FormDonasi()

    # response = {'program' : details[0],'form' : form}

    # return render(request, 'submitForm/submitform.html', response)

def listdonatur_func(request, program=None):
    dataListDonatur = Donasi.objects.all().filter(nama_program = program).order_by('-datetime')


    response = {'data':dataListDonatur, 'program':program}
    return render(request, "submitForm/listdonatur.html", response)